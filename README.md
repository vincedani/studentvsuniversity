Megvalósított opciók:
    App:
     - Kicsi közepes Tile ikonok/Splash Screen: 3
     - file/db műveletek: 5
     - Local/Remote Storage: 3
     
    UI:
     - Design: 3
     - Listás megjelenítés: 3
     
    Settings:
     - Menü: 4
     - Egy beállítási tényező támogatása: 6
     
    Input: 
     - Billentyűzet: 2
     
    Windows Store-ba publikálás:
     - Storba beküldött elfogadott program: 15
     
------------------------------------------------
Remélt pontok: 44